package utfpr.ct.dainf.if62c.pratica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leandrocarvalho
 */
public class Circulo extends Elipse{

    public Circulo(double semieixoR) {
        super(semieixoR, semieixoR);
    }
  
    @Override
    public double getPerimetro() {
        return Math.PI*getR()*2;        
    }
    
}
